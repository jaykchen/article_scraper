pub struct Progress {
    pub total_size: usize,
    pub downloaded: usize,
}
